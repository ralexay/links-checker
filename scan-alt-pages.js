const { getRows } = require('./lib/db');
const altPages = require('./lib/alt-pages');
const urlScanner = require('./lib/scanners/url-scanner');

const testedHost = 'www.top10.com';

async function hasPreviousResultsForUI(uiName) {
  const cmd = `
    select 
    count(host) as count
    from pages
    where host='localhost:6003'
    and uiName='${uiName}'`;
  const rows = await getRows(cmd);
  return rows[0].count > 0;
}

async function getUiNamesAndVersions() {
  const cmd = `select 
  uiName,
  uiVersion
  from (select * from pages where uiName is not null and host = '${testedHost}' )
  group by 
  uiName,
  uiVersion`;

  const rows = await getRows(cmd);
  return rows;
}

async function getPaths(uiName) {
  const cmd = ` select host, pathname from pages where uiName ='${uiName}' and host = '${testedHost}' group by host, pathname `;
  const rows = await getRows(cmd);
  return rows;
}

(async () => {
  const uis = await getUiNamesAndVersions();
  const urls = [];
  for (let i = 0; i < uis.length; i++) {
    const { uiName, uiVersion } = uis[i];
    const hasPreviousVersion = await hasPreviousResultsForUI(uiName);
    if (!hasPreviousVersion) {
      const rows = await getPaths(uiName);
      rows.forEach(row => {
        const hash = altPages[uiName];
        if (hash) {
          const url = `http://localhost:6003${
            row.pathname
          }?altTypeHash=${hash}`;
          urlScanner.enqueue(url);
          urls.push(url);
        }
      });
    }
  }
  //   console.log(urls);
})();
