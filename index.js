const { createTable, dropTable } = require('./lib/db');
const urlScanner = require('./lib/scanners/url-scanner');
const siteScanner = require('./lib/scanners/site-scanner');
const crypto = require('crypto');

(async () => {
  await dropTable();
  await createTable();

  urlScanner.enqueue(
    'http://localhost:6003/dna-testing?altTypeHash=d122d4dc59596fe4724efaccf86ca234b5eef089482bb5f5d2a6aba47ec9f1adcaa2369d6ce7debb5ff2434a97dc0986',
    'www.top10.com'
  );
  urlScanner.enqueue('https://www.top10.com/dna-testing', 'www.top10.com');
})();
