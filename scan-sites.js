const { createTable } = require('./lib/db');
const siteScanner = require('./lib/scanners/site-scanner');

(async () => {
  await createTable();

  siteScanner.enqueue(
    'https://www.top10bestbusinessloans.com',
    'www.top10bestbusinessloans.com'
  );
  siteScanner.enqueue(
    'https://www.top10insuranceoffers.com',
    'www.top10insuranceoffers.com'
  );
  siteScanner.enqueue('https://www.top10.com', 'www.top10.com');
})();
