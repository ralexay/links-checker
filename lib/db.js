const sqlite3 = require('sqlite3');

const db = new sqlite3.Database('results.db');

const dbCommands = {
  runCommand: async command => {
    return new Promise((resolve, reject) => {
      db.run(command, function(err) {
        if (err) {
          reject(err);
        }
        resolve();
      });
    });
  },

  getRows: async command => {
    return new Promise((resolve, reject) => {
      db.all(command, [], (err, rows) => {
        if (err) {
          reject(err);
        }
        resolve(rows);
      });
    });
  },

  dropTable: async () => {
    return dbCommands.runCommand('DROP TABLE IF EXISTS pages;');
  },

  createTable: async () => {
    const command = `CREATE TABLE IF NOT EXISTS pages(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            host TEXT,
            pathname TEXT,
            search TEXT,
            linkUrl TEXT,
            internal TEXT,
            broken TEXT,
            uiName TEXT,
            uiVersion TEXT,
            json TEXT,
            md5 TEXT
        );`;

    return dbCommands.runCommand(command);
  },

  /**
   * @param {Array<string>} items row columns (pageUrl, linkUrl, internal, broken, json, uiName, uiVersion)
   * @returns Promise
   */
  addRow: async items => {
    const command = `insert into pages (host, pathname, search, linkUrl, internal, broken, json, md5, uiName, uiVersion) values(?,?,?,?,?,?,?,?,?,?)`;
    return new Promise((resolve, reject) => {
      db.run(command, items, function(err) {
        if (err) {
          reject(err);
        }
        resolve(this.lastID);
      });
    });
  },

  getUniquePages: async () => {
    const command = `SELECT pageUrl, uiName, uiVersion FROM pages GROUP BY pageUrl, uiName, uiVersion`;
    return dbCommands.getRows(command);
  }
};

module.exports = dbCommands;
