const blc = require('broken-link-checker');
const { cloneDeep } = require('lodash');
const { parseLinkResult, savePageResult, commonOptions } = require('./common');
const { logger } = require('../logger');

const records = [];

var siteChecker = new blc.SiteChecker(
  { ...commonOptions },
  {
    // html: function(tree, robots, response, pageUrl, customData){},
    link: function(result, customData) {
      const res = parseLinkResult(result, customData);
      if (res) {
        records.push(res);
      }
    },
    page: async function(error, pageUrl, customData) {
      logger.info(`finished page: ${pageUrl} found links: ${records.length}`);

      await savePageResult(error, pageUrl, customData, cloneDeep(records));

      records.length = [];
    },
    site: function(error, siteUrl, customData) {
      logger.info(`done site ${siteUrl}`);
    },
    end: function() {
      logger.info('end');
    }
  }
);

module.exports = siteChecker;
