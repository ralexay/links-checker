const blc = require('broken-link-checker');
const { cloneDeep } = require('lodash');
const { parseLinkResult, savePageResult, commonOptions } = require('./common');
const { logger } = require('../logger');

const records = [];
const urlScanner = new blc.HtmlUrlChecker(
  { ...commonOptions },
  {
    link: (result, customData) => {
      const res = parseLinkResult(result, customData);
      if (res) {
        records.push(res);
      }
    },
    page: async (error, pageUrl, customData) => {
      logger.info(`finished page: ${pageUrl} found links: ${records.length}`);

      await savePageResult(error, pageUrl, customData, cloneDeep(records));

      records.length = [];
    },
    end: () => {
      logger.info(`end page parse`);
    }
  }
);

module.exports = urlScanner;
