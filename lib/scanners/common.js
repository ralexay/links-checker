const { getPageUIData } = require('../page-utils');
const { addRow } = require('../db');
const crypto = require('crypto');

function md5(text) {
  return crypto
    .createHash('md5')
    .update(text)
    .digest('hex');
}

function linkResultParser(result) {
  // const pageUrl = result.base.resolved;
  const { host, pathname, search } = result.base.parsed;
  const linkUrl = result.url.original;
  const { internal, broken, html } = result;
  const element = html.tag;
  let linkEntension = null;
  if (linkUrl.endsWith('.svg')) {
    linkEntension = (linkUrl.match(/\.([^.]*?)(?=\?|#|$)/) || [])[1];
  }
  if (internal && !linkEntension) {
    return {
      md5: md5(html.selector),
      host,
      pathname,
      search,
      linkUrl,
      internal,
      broken,
      json: element,
      linkEntension
    };
  }
  return null;
}

function parseLinkResult(result, customData) {
  const res = linkResultParser(result);
  if (res) {
    const {
      host,
      pathname,
      search,
      linkUrl,
      internal,
      broken,
      json,
      linkEntension,
      md5
    } = res;
    if (internal && !linkEntension) {
      return {
        host,
        pathname,
        search,
        linkUrl,
        internal: `${internal}`,
        broken: `${broken}`,
        json,
        md5
      };
    }
  }
  return null;
}

async function savePageResult(error, pageUrl, customData, localRecords) {
  const uiData = await getPageUIData(pageUrl);
  const dbPromises = [];
  localRecords.forEach(rec => {
    rec.page = pageUrl;
    dbPromises.push(
      addRow([
        rec.host,
        rec.pathname,
        rec.search,
        rec.linkUrl,
        rec.internal,
        rec.broken,
        rec.json,
        rec.md5,
        uiData.name,
        uiData.version
      ])
    );
  });

  console.log('before all promises');
  await Promise.all(dbPromises);
  console.log('after all promises');
}

module.exports = {
  parseLinkResult,
  savePageResult,
  commonOptions: {
    cacheResponses: true,
    excludeExternalLinks: true,
    excludeInternalLinks: false,
    excludeLinksToSamePage: true,
    filterLevel: 1,
    honorRobotExclusions: false,
    maxSocketsPerHost: 10
  }
};
