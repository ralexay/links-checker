const request = require('request');
const cheerio = require('cheerio');
const { logger } = require('./logger');

async function getPageUIData(url) {
  return new Promise((resolve, reject) => {
    request.get(url, (err, response, body) => {
      if (err) {
        reject(err);
      }
      let name = null;
      let version = null;
      try {
        const $ = cheerio.load(body);
        version = $('body').data('version');
        name = $('body').data('name');
      } catch (e) {
        logger.error(`cheerio failed on page ${url},  ${e}`);
      }
      resolve({
        name,
        version,
        url
      });
    });
  });
}

module.exports = {
  getPageUIData
};
