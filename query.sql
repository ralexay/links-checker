SELECT
	A.pathname, A.linkUrl, A.json, (A.linkUrl = B.linkUrl) as equal,  B.pathname, B.linkUrl, B.json
FROM (
	SELECT
		pathname,
		linkUrl,
		uiVersion,
		json
	FROM
		pages
	WHERE
		uiVersion = '0.0.1-react.144'
	GROUP BY
		pathname,
		linkUrl,
		uiVersion,
		json) AS A ,
	
	(
	SELECT
		pathname,
		linkUrl,
		uiVersion,
		json
	FROM
		pages
	WHERE
		uiVersion = '0.0.1-react.243'
	GROUP BY
		pathname,
		linkUrl,
		uiVersion,
		json) AS B

		
		
		
	-- ##  where uiVersion='0.0.1-react.243'